#include <QCoreApplication>
#include <QFile>
#include <QTextStream>
#include <QDebug>
QString sasator (QString template_path, QStringList &lines){
    QString file_example = R"(
                           <body>
                           %ФАМИЛИЯ% %ИМЯ% %не_распознать%
                           </body>
                           )";


    QChar special('%');//разделитель
    QMap<QString, QString> map;// набор возможно встречающихся символов
    map["ФАМИЛИЯ"] = "Будько";
    map["ИМЯ"] = "Иван";
    QString result;
    QString tag;
    enum class State {INIT, CLOSE} state = State::INIT;//КА

    QFile inputFile(template_path);
    if (inputFile.open(QIODevice::ReadOnly)){
        QTextStream in(&inputFile);
        while (!in.atEnd()){
            QString line = in.readLine();
            ////////
            for(int i  = 0; i < line.length(); ++i) {
                QChar ch = line[i];
                switch(state) {
                case State::INIT:
                    if( ch == special ) {
                        state = State::CLOSE;
                    } else {
                        result += ch;
                    }
                    break;
                case State::CLOSE:
                    if( ch == special ) {
                        auto it = map.find(tag);
                        if( it != map.end() ) {
                            result += it.value();
                        } else {
                            result += "NOT FOUND";
                        }
                        state = State::INIT;
                        tag.clear();
                    } else {
                        tag += ch;
                    }
                }
            }
            result += '\n';
            ////////
        }
        inputFile.close();
    }


    //qDebug() << result;
    return result;

}
//заполняет лист ключами
QString anallizator (QString template_path, QStringList &list) {
    QString result;
    QString tag;
    QChar special = '%';
    enum class State {INIT, CLOSE} state = State::INIT;//КА

    QFile inputFile(template_path);
    if (inputFile.open(QIODevice::ReadOnly)){
        QTextStream in(&inputFile);
        while (!in.atEnd()){
            QString line = in.readLine();
            ////////
            for(int i  = 0; i < line.length(); ++i) {
                QChar ch = line[i];
                switch(state) {
                case State::INIT:
                    if( ch == special ) {
                        state = State::CLOSE;
                    } else {
                        result += ch;
                    }
                    break;
                case State::CLOSE:
                    if( ch == special ) {
                        //                      auto it = map.find(tag);
                        //                      if( it != map.end() ) {
                        //                          result += it.value();
                        //                      } else {
                        //                          result += "NOT FOUND";
                        //                      }
                        list.append(tag);
                        state = State::INIT;
                        tag.clear();
                    } else {
                        tag += ch;
                    }
                }
            }
            result += '\n';
            ////////
        }
        inputFile.close();
    }
    return  "result";
}

QString generator (QString template_path, QStringList &values) {
    QString result;
    QString tag;
    QChar special = '%';
    enum class State {INIT, CLOSE} state = State::INIT;//КА
    int iter = 0;//итератор по values
    QFile inputFile(template_path);
    if (inputFile.open(QIODevice::ReadOnly)){
        QTextStream in(&inputFile);
        while (!in.atEnd()){
            QString line = in.readLine() + "\n";
            ////////
            for(int i  = 0; i < line.length(); ++i) {
                QChar ch = line[i];
                switch(state) {
                case State::INIT:
                    if( ch == special ) {
                        state = State::CLOSE;
                    } else {
                        result += ch;
                    }
                    break;
                case State::CLOSE:
                    if( ch == special ) {
                        result += values[iter++];
                        state = State::INIT;
                        tag.clear();
                    } else {
                        tag += ch;
                    }
                }
            }
            //result += '\n';
            ////////
        }
        inputFile.close();
    }
    return  result;
}


QString html_concat(QString html1,QString html2, int lines_on_page = 52){
    int page1_counter = html1.split("\n").size();
    int page2_counter = html2.split("\n").size();

    if (page1_counter == lines_on_page && page2_counter == lines_on_page) return html1 + html2;
    if(lines_on_page > page1_counter){
        html1 += "\n<br />";
        for (int i = 0; i < lines_on_page - page1_counter; i++) {
            html1 += "\n<br />";
        }
    }
    if(lines_on_page > page2_counter){
        html1 += "\n<br />";
        for (int i = 0; i < lines_on_page - page2_counter; i++) {
            html2 += "\n<br />";
        }
    }
    return html1 + html2;

}




int main(int argc, char *argv[]){
    QStringList  list /*= {"sas","sus"}*/;
    qDebug()<<"-----analizator-----";
    anallizator("otchet.html",list);
    for (auto a : list) qDebug()<< a<< " ";
    QStringList  list2 = {"Матросов","Егор","Андреевич","Матросов","Егор","Андреевич","Матросов","Егор","Андреевич"};

    qDebug()<<"\n-----genherator-----\n";
    QString str = generator("otchet.html",list2);

    QString filename = "Result.html";
        QFile file(filename);
        if (file.open(QIODevice::ReadWrite)) {
            QTextStream stream(&file);
            stream << str << endl;
        }

    //QCoreApplication a(argc, argv);

    //return a.exec();
}
